import java.util.ArrayList;
import java.util.Collection;

public abstract class Bandeja {
	
	protected CuentaDeCorreo cuentaAsociada;
	protected Collection<Email> emails;
	
	public Bandeja(CuentaDeCorreo cuentaAsociada)
	{
		this.cuentaAsociada = cuentaAsociada;
		this.emails = new ArrayList<Email>();
	}
	
	public void depositar(Email email)
	{
		this.emails.add(email);
	}
	
	public void eliminar(Email email)
	{
		// Paso 1: saco el email de esta bandeja
		this.emails.remove(email);
				
		// Paso 2: le pido a la cuenta que lo deposite en la papelera
		this.cuentaAsociada.depositarEnLaPapelera(email);
	}
	
	public boolean contiene(Email email)
	{
		return this.emails.contains(email);
	}
	
	public int contabilizar()
	{
		return this.emails.size();
	}

}
