
public class Papelera extends Bandeja {

	public Papelera(CuentaDeCorreo cuentaAsociada) {
		super(cuentaAsociada);
	}
	
	public void eliminar(Email email)
	{
		this.emails.remove(email);
	}

	public void vaciar() {
		this.emails.clear();		
	}

}
