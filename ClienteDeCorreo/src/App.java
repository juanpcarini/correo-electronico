
public class App {

	public static void main(String[] args) {
		CuentaDeCorreo cuenta1 = new CuentaDeCorreo("juanitonelli@gmail.com");
		CuentaDeCorreo cuenta2 = new CuentaDeCorreo("pepe@gmail.com");
		CuentaDeCorreo cuenta3 = new CuentaDeCorreo("maria@gmail.com");
		
		Email email = new Email(cuenta1, "Hola", "Como andas?");
		email.agregarDestinatario(cuenta2);
		email.agregarDestinatario(cuenta3);
		
		email.confirmarEnvio();
		
		cuenta1.eliminar(email);
		cuenta1.eliminar(email);
		
		System.out.println("FIN");
	}

}
