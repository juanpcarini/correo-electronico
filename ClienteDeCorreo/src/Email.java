import java.util.ArrayList;
import java.util.Collection;

public class Email {
	
	private String asunto;
	private String cuerpo;
	
	private CuentaDeCorreo remitente;
	private Collection<CuentaDeCorreo> destinatarios;
	
	public Email(CuentaDeCorreo remitente, String asunto, String cuerpo)
	{
		this.asunto = asunto;
		this.cuerpo = cuerpo;
		this.remitente = remitente;
		this.destinatarios = new ArrayList<CuentaDeCorreo>();
	}
	
	public void agregarDestinatario(CuentaDeCorreo nuevoDestinatario)
	{
		this.destinatarios.add(nuevoDestinatario);
	}
	
	public void confirmarEnvio()
	{
		// Verifico que haya al menos un destinatario
		if (!this.destinatarios.isEmpty())
		{
			// Poner el email en la bandeja de salida del remitente
			this.remitente.enviar(this);
			
			// Poner el email en la bandeja de entrada de todos los destinatarios
			for (CuentaDeCorreo destinatario : this.destinatarios) {
				destinatario.recibir(this);
			}
		}
		
		// En caso de que no haya ningun destinatario
		else
		{
			System.out.println("No se puede enviar un email sin al menos un destinatario.");
		}
	}

	public String getAsunto() {
		return asunto;
	}

	public String getCuerpo() {
		return cuerpo;
	}
	
	
}
