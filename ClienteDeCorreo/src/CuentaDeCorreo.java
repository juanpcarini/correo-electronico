
public class CuentaDeCorreo {
	
	// Direcci�n de correo electr�nico que identifa a la cuenta
	private String direccion;
	
	// Bandejas
	private BandejaDeEntrada bandejaEntrada;
	private BandejaDeSalida bandejaSalida;
	private Papelera papelera;
	
	public CuentaDeCorreo(String direccion)
	{
		this.direccion = direccion;
		this.bandejaEntrada = new BandejaDeEntrada(this);
		this.bandejaSalida = new BandejaDeSalida(this);
		this.papelera = new Papelera(this);
	}
	
	public void recibir(Email email)
	{
		this.bandejaEntrada.depositar(email);
	}
	
	public void enviar(Email email)
	{
		this.bandejaSalida.depositar(email);
	}
	
	public void eliminar(Email email)
	{
		// Verificar si esta en la bandeja de entrada o salida
		if (this.bandejaEntrada.contiene(email))
		{			
			this.bandejaEntrada.eliminar(email);
		}
		
		// Verifico bandeja de salida
		else if (this.bandejaSalida.contiene(email))
		{			
			this.bandejaSalida.eliminar(email);
		}
		
		// Verifico papelera
		else if (this.papelera.contiene(email))
		{			
			this.papelera.eliminar(email);
		}
		
		else
		{
			System.out.println("El mail al eliminar no est� en la cuenta.");
		}
	}
	
	public void vaciarPapelera()
	{
		this.papelera.vaciar();
	}
	
	public int contabilizarEmailsNoEliminados()
	{
		return this.bandejaEntrada.contabilizar() + this.bandejaSalida.contabilizar();		
	}
	
	public void listarRecibidos()
	{
		//TODO
	}
	
	public void listarEnviados()
	{
		//TODO
	}
	
	public void listarPapelera()
	{
		//TODO
	}

	public void depositarEnLaPapelera(Email email) {
		this.papelera.depositar(email);
	}
}
